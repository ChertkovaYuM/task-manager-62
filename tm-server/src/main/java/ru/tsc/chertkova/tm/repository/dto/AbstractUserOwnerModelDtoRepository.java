package ru.tsc.chertkova.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.tsc.chertkova.tm.dto.model.AbstractUserOwnerModelDTO;

@Repository
public interface AbstractUserOwnerModelDtoRepository<M extends AbstractUserOwnerModelDTO>
        extends AbstractDtoRepository<M> {

}
