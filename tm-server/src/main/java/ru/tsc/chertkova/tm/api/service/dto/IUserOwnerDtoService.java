package ru.tsc.chertkova.tm.api.service.dto;

import ru.tsc.chertkova.tm.dto.model.AbstractUserOwnerModelDTO;

public interface IUserOwnerDtoService<M extends AbstractUserOwnerModelDTO> extends IDtoService<M> {
}
